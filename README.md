# BitCryptoCompare

**A Python wrapper for CryptoCompare API by Bitmast Digital Services.** 

BitCryptoCompare is designed to be a Python wrapper of the CryptoCompare API. 
 
## Motivation
The advent of cryptocurrency in the financial and other payment systems makes it very pressing to have access to timely and valid information about cryptocurrency. Added to this challenge is the fact that cryptocurrencies have high volatility which can swing investors from huge profits to losses within a fraction of seconds. 
The story of cryptocurrency is far from its epilogue as new tools and a "killer application" is still being researched and pushed out to the public market. Despite all the progress currently registered, an average computer user often considers cryptocurrency and the entire blockchain domain to be an industry shrouded in darkness. This barrier for users can be reduced by improving both the interface by which blockchain technologies are accessed and simplifying the information needed by users to make valuable decisions.

Bitmast is poised to producing a simple yet effective tool by which blockchain transactions and related data could be accessed by users. One of the far-reaching goals for this tool is to enable people to access information without heavy cognitive overloads incurred from viewing many charts, numbers, and hashed strings. An integral component for attaining this is a reliable source of cryptocurrency data. We considered the CryptoCompare API a rich and reliable source by which cryptocurrency information can be extracted from data for personal and research purposes.

Aside from this, BitCryptoCompare aims to be a simple framework that can be integrated with other Python-based frameworks such as Django, Flask etc.
 
## Description
The BitCryptoCompare is a Python wrapper supporting multiple endpoints of the CryptoCompare API. Some of the key innovative features of the design include:

1. API calls are through callable objects that allow storing and forwarding HTTP requests as objects through the system. 
Example to initiate a request for data, simply follow the steps:
 
~~~python
# Initiate a request
from bit_cryptocompare.util.url_factory import single_symbol_price
from bit_cryptocompare.parsers import SingleSymbolPrice

server_response = single_symbol_price('BTC', 'XLM,LTC,DASH', try_conversion=True,
                              relaxed_validation=True,
                              exchange='Kraken',
                              extras='Bitmast Explorer', sign=False)
data = SingleSymbolPrice.parse(server_response)                
 # further process server_response or return to caller
 
~~~ 

2. Application-specific requirements and account details can be managed from a centralized configuration system. This will enable developers to extend the project for their peculiar needs.

3. A set of data validation is provided for selected request object/data being passed to the CryptoCompare API. Furthermore, the default data validation can be replaced or extended by the developer to suit their target need. 
It is believed, however, that default validation meets the requirements 
for most cases.

4. A `parser` module is provided for converting responses from the 
CryptoCompare server into target native system objects. Responses from the server can also be converted into native dictionary objects or passed along to calling a function.

5. All HTTP requests are made through the Python `requests` API. 
See documentation [here](https://2.python-requests.org/en/master/) 
for more information.
 
## Dependencies
The BitCryptoCompare requires `requests` which can be downloaded 
[here](https://pypi.org/project/requests/). 

The Python language version for current implementation is Python 3.6.8 
 
## Build Status
**Version: 1.0.0**

Current development of BitCryptoCompare is version 1.0.0 
This is considered the Alpha Edition. 
 
## Features
The current edition covers the following API endpoints.

| API Endpoints |
| ------------- |
| Single Symbol Price |  
| Multiple Symbols Price |  
| Multiple Symbols Full Data |  
| Generated Custom Average |  
| Latest News Articles |  
| List News Feeds |  
| News Article Categories |  
| List News Feeds and Categories |  
| Exchanges With Order Book Data |  
| Order Book L1 Top |  
| Order Book L2 Snapshot |  
| All the Exchanges and Trading Pairs |  
| All the Coins |  
| All Exchanges General Info |  


Information about the CryptoCompare API can be found 
[here](https://min-api.cryptocompare.com/documentation). 

## Contributing
Please visit application repo for further information on extending the project. 
Ideas and comments are welcome. 
 
## Author
Current development is by Aniefiok Friday 
[@frier17](https://gitlab.com/frier17). 


Copyright 2019 @frier17


