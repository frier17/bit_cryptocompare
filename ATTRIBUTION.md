# The Bitmast Explorer
*An online price comparative tool based on the CryptoCompare web services*

## Author
Website content and design are managed by Bitmast Digital Services, Editorial team.

Information used on the website are contents adopted from CryptoCompare and any other listed partner, content creator, contributor, or source. 

## Source
View detail information on the [CryptoCompare](http://www.cryptocompare.com) website

## Terms of Usage
Application is designed as a not for profit tool. Appropriate usage in line with legal constraints of resource owners
 and third parties should be respected. For further details, see terms of usage 
 [here](http://www.cryptocompare.com/terms-conditions/).

## License
The Bitmast Explorer is licensed under [Apache License, Version 2.0, January 2004](http://www.apache.org/licenses/).
Resource from CryptoCompare are protected under the listed policies and terms of the company. See terms of usage for
 details.
 
## Disclaimer
The information contained in this website is for general information purposes only. The information is provided by
 Crypto Coin Comparison LTD and any other listed source. Use information "as is" and not as a financial or investment
  tool. We make no representations or warranties of any kind, express or implied, about the completeness, accuracy
  , reliability, suitability or availability with respect to the website or the information, products, services, or
   related graphics contained on the website for any purpose. Any decision based on information or resources on
    this website or related tools and services of this website is purely on user's own risk. 
    
Although, CryptoCompare is referenced in the Bitmast Explorer, there is no legal obligations or recommendation by
 CryptoCompare of Bitmast Explorer.
 
&copy; Bitmast Digital Services, 2019